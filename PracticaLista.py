import glob

lista=list()
'''
La funcion que servirá de menu
'''

def Lista ():
    op=int(input("\n*****Lista UTN*****\n"
            "1. Insertar elementos a la lista\n"
            "2. Recorrer lista\n"
            "3. Consultar en lista\n"
            "4. Modificar un elemento a la lista\n"
            "5. Elimiar un elemento\n"
            "6. Eliminazr toda la lista\n"
            "7. Salir\n"
            "Ingrese lo que desea escoger : \n"))
def documentacion ():
    print(Lista.__doc__)


    while op == 1:
        '''
        Todo lo que está dentro de aquí son para las funcionalidades del menu dentro del otro menu
        '''

        menu = int(input("\n*****Lista UTN*****\n"
                         "1. Insertar elementos a la lista al final\n"
                         "2. Insertar un elemento en una posicion es especifico\n"
                         "3. Volver al menu principal\n"))
        if menu ==1:
            ins_nums = int(input("Ingrese el elemento: "))
            lista.append(ins_nums)
            print(lista)
        elif menu ==2:
            '''
            En este modo, el usuario puede insertar un numero en la posicion que el desee, mediante
            el numero de posicion en lista y el numero que desea insertar.
            '''

            pos = int(input("Ingrese la pocision en la que desea guardar el elemento : "))
            ns_nums = int(input("Ingrese el elemento: "))
            lista[pos]=ns_nums
            print(lista)
        elif menu ==3:
            Lista()
    if op == 2:
        '''
        Aqui solo imprimirá la lista demiante un for
        '''
        for i in lista:
            print(i,end="-")
    elif op == 3:
        '''
        En esta opcion el usuario ingresa que valor desea consultar y si este se encuentra, le dirá que si se encuentra y tambien en que
        posicion de la lista se encuentra
        '''

        consul = int(input("Que elemento desea consultar en la lista? : "))
        if consul in lista:
            print("El elemento introducido se encuentra en la posicion {}".format(lista.index(consul)))
        else:
            print("Este elemento no se encuentran en la lista!")
    elif op == 4:
        '''
        En esta opcion al usuario se le pide la posicion del numero en la que desea modificar
        y el nuevo numero por el que va a sustituir
        '''

        pos = int(input("En que posicion de la lista desea modificar? :"))
        mod = int(input("Que numero desea desea moficar en esa posicion? :"))
        for i in lista:
            i = pos
            valor = mod

            lista[pos] = valor
            print(lista)
            break


    elif op == 5:
        '''
        En esta opcion se le deja al usuario cual numero de la lista desea eliminar
        '''

        elem = int(input("Que elemento de la lista desea eliminar ? : "))
        lista.pop(elem)
        print(lista)

    elif op == 6:
        '''
        Borra todo lo que el usuario ya haya insertado, dejando la lista vacia de nuevo
        '''

        lista.clear()
        print(lista)
    elif op == 7:
        '''
        Aqui simplemente demuestra un mensaje de salida y termina el proceso
        '''

        print("Gracias por usar nuestra aplicacion!")
        pass

    else:
        print("El numero ingresado no existe!")

Lista()


